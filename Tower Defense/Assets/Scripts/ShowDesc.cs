﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShowDesc : MonoBehaviour {

    public Text towerDesc;

    public void ShowDescription()
    {
        towerDesc.text = this.GetComponent<Tower>().towerDesc;
    }
}
