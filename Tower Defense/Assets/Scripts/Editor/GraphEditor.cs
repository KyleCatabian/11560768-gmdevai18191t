﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.Linq;

[CustomEditor(typeof(Graph))]
public class GraphEditor : Editor
{

    protected Graph m_Graph;
    protected Node m_From;
    protected Node m_To;
    protected Follower m_Follower;
    protected Path m_Path = new Path();

    private void OnEnable()
    {
        m_Graph = target as Graph;
    }

    private void OnSceneGUI()
    {
        if (m_Graph == null) return;

        for (int i = 0; i < m_Graph.GetNodes.Count; i++)
        {
            Node node = m_Graph.GetNodes[i];
            for (int j = 0; j < node.GetConnections.Count; j++)
            {
                Node connection = node.GetConnections[j];
                if (connection == null)
                {
                    continue;
                }

                float distance = Vector3.Distance(node.transform.position, connection.transform.position);
                Vector3 difference = connection.transform.position - node.transform.position;
                Handles.Label(node.transform.position + (difference / 2), distance.ToString(), EditorStyles.whiteBoldLabel);
                if (m_Path.GetNodes.Contains(node) && m_Path.GetNodes.Contains(connection))
                {
                    Color color = Handles.color;
                    Handles.color = Color.green;
                    Handles.DrawLine(node.transform.position, connection.transform.position);
                    Handles.color = color;
                }
                else
                {
                    Handles.DrawLine(node.transform.position, connection.transform.position);
                }
            }
        }
    }

    public override void OnInspectorGUI()
    {
        m_Graph.GetNodes.Clear();
        foreach (Transform child in m_Graph.transform)
        {
            Node node = child.GetComponent<Node>();
            if (node != null)
            {
                m_Graph.GetNodes.Add(node);
            }
        }

        base.OnInspectorGUI();
        EditorGUILayout.Separator();

        m_From = (Node)EditorGUILayout.ObjectField("From", m_From, typeof(Node), true);
        m_To = (Node)EditorGUILayout.ObjectField("To", m_To, typeof(Node), true);
        m_Follower = (Follower)EditorGUILayout.ObjectField("Follower", m_Follower, typeof(Follower), true);

        if (GUILayout.Button("Show Shortest Path"))
        {
            m_Path = m_Graph.GetDijkstraPath(m_From, m_To);

            if (m_Follower != null)
            {
                m_Follower.FollowPath(m_Path);
            }
            SceneView.RepaintAll();
        }
    }
}