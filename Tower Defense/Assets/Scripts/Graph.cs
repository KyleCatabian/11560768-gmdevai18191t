﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Graph : MonoBehaviour
{
    [SerializeField]
    protected List<Node> nodes = new List<Node>();

    public virtual List<Node> GetNodes
    {
        get { return nodes; }
    }

    public virtual Path GetDijkstraPath(Node start, Node end)
    {
        if (start == null || end == null)
        {
            throw new ArgumentNullException();
        }

        Path path = new Path();

        if (start == end)
        {
            path.GetNodes.Add(start);
            return path;
        }

        List<Node> unvisited = new List<Node>();

        Dictionary<Node, Node> previous = new Dictionary<Node, Node>();

        Dictionary<Node, float> distances = new Dictionary<Node, float>();

        for (int i = 0; i < nodes.Count; i++)
        {
            Node node = nodes[i];
            unvisited.Add(node);

            distances.Add(node, float.MaxValue);
        }

        distances[start] = 0f;

        Node traveller = unvisited[0];

        while (unvisited.Count != 0)
        {
            unvisited = unvisited.OrderBy(node => distances[node]).ToList();

            Node current = unvisited[0];

            unvisited.Remove(current);

            if (current == end)
            {
                while (previous.ContainsKey(current))
                {
                    path.GetNodes.Insert(0, current);

                    current = previous[current];
                }
                path.GetNodes.Insert(0, current);
                break;
            }

            for (int i = 0; i < current.GetConnections.Count; i++)
            {
                Node neighbor = current.GetConnections[i];

                float length = Vector3.Distance(current.transform.position, neighbor.transform.position);

                float alt = distances[current] + length;

                if (alt < distances[neighbor])
                {
                    distances[neighbor] = alt;
                    previous[neighbor] = current;
                    traveller = current;
                }
            }
        }

        if (end.GetConnections.Count != 0)
        {
            if (traveller != end.GetConnections[0])
            {
                path = null;
                return path;
            }
        }
        else if (end.GetNotConnections.Count != 0)
        {
            if (traveller != end.GetNotConnections[0])
            {
                path = null;
                return path;
            }
        }

        path.Bake();
        return path;
    }
}