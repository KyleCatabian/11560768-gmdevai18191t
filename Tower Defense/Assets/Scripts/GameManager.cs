﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {

    public Graph graph;
    public Node start;
    public Node end;
    public Button restartMazeBtn;
    public Button nextBtn;
    public Button startGameBtn;
    public Button dmgTowerBtn;
    public Button slowTowerBtn;
    public Button buildTowerBtn;
    public Button nextWaveBtn;
    public Text gameInfo;
    public GameObject dmgTowerPrefab;
    public GameObject slowTowerPrefab;
    public string gameState;
    public int towersToPlace;
    public Node selectedNode;
    public GameObject selectedTower;
    public GameObject spawner;


    protected Path path = new Path();

    public bool isMazeValid;
    public int currentWave;
    public int currentLives;
    public int enemiesLeft;

    private bool hasGameStarted;

    // Use this for initialization
    void Start () {
        gameState = "Build Maze";
        currentWave = 0;
        currentLives = 20;
        towersToPlace = 0;
        startGameBtn.gameObject.SetActive(false);
        dmgTowerBtn.gameObject.SetActive(false);
        slowTowerBtn.gameObject.SetActive(false);
        buildTowerBtn.gameObject.SetActive(false);
    }
	
	// Update is called once per frame
	void Update () {
        UpdateValues();
        if (hasGameStarted && enemiesLeft == 0 && currentWave % 2 == 0)
        {
            gameState = "Build Towers";
            towersToPlace++;
            hasGameStarted = false;
            dmgTowerBtn.gameObject.SetActive(true);
            slowTowerBtn.gameObject.SetActive(true);
            buildTowerBtn.gameObject.SetActive(true);
        }
	}

    public void EvaluateMaze()
    {
        path = graph.GetDijkstraPath(start, end);
        if (path != null)
        {
            isMazeValid = true;
            gameState = "Build Towers";
            towersToPlace = 3;
            startGameBtn.gameObject.SetActive(true);
            nextBtn.gameObject.SetActive(false);
        }
        else
        {
            isMazeValid = false;
        }
    }

    public void SetSelectedTower(GameObject obj)
    {
        this.selectedTower = obj;
    }

    public void BuildTower()
    {
        if (selectedNode != null && selectedTower != null && towersToPlace != 0 && selectedNode.isTower == false)
        {
            Instantiate(selectedTower, selectedNode.transform);
            if (selectedTower.gameObject.name == "Damage Tower")
            {
                selectedNode.GetComponent<SpriteRenderer>().color = Color.red;
            }
            else if (selectedTower.gameObject.name == "Slow Tower")
            {
                selectedNode.GetComponent<SpriteRenderer>().color = Color.blue;
            }
            selectedNode.isTower = true;
            towersToPlace--;
            selectedNode = null;
        }
    }

    public void StartGame()
    {
        if (towersToPlace == 0)
        {
            restartMazeBtn.gameObject.SetActive(false);
            dmgTowerBtn.gameObject.SetActive(false);
            slowTowerBtn.gameObject.SetActive(false);
            buildTowerBtn.gameObject.SetActive(false);
            startGameBtn.gameObject.SetActive(false);
            nextWaveBtn.gameObject.SetActive(true);

            gameState = "Game";
            spawner.gameObject.SetActive(true);
            currentWave = 1;
            hasGameStarted = true;
        }
    }

    public void NextWave()
    {
        if (enemiesLeft == 0 && towersToPlace == 0)
        {
            currentWave++;
            spawner.gameObject.SetActive(true);
        }
    }

    void UpdateValues()
    {
        gameInfo.text = gameState + "\n\n" + currentWave + "\n\n" + currentLives + "\n\n" + towersToPlace;
    }
}
