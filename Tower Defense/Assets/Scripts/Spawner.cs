﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour {

    public Graph graph;
    public Node start;
    public Node end;
    public GameObject gameManager;
    public GameObject enemyPrefab;
    public float SpawnInterval;
    public int SpawnCount;

    private int counter;
    private List<GameObject> pooledEnemies = new List<GameObject>();

    // Use this for initialization
    void Start () {
	}

    private void OnEnable()
    {
        StartCoroutine(SpawnEnemies());
    }

    // Update is called once per frame
    void Update () {
        SpawnCount = 4 + gameManager.GetComponent<GameManager>().currentWave;
        if (counter > SpawnCount)
        {
            counter = 0;
            this.gameObject.SetActive(false);
        }
	}

    IEnumerator SpawnEnemies()
    {
        while (counter <= SpawnCount)
        {
            SpawnEnemy();
            counter++;
            yield return new WaitForSeconds(SpawnInterval);
        }
    }

    private void SpawnEnemy()
    {
        GameObject obj = Instantiate(enemyPrefab);
        obj.GetComponent<Enemy>().gameManager = gameManager;
        obj.GetComponent<Enemy>().speed = 0.01f + gameManager.GetComponent<GameManager>().currentWave * 0.01f;
        obj.GetComponent<Enemy>().health = 10 * gameManager.GetComponent<GameManager>().currentWave;
        obj.GetComponent<Follower>().graph = graph;
        obj.GetComponent<Follower>().start = start;
        obj.GetComponent<Follower>().end = end;
        obj.GetComponent<Follower>().speed = obj.GetComponent<Enemy>().speed;
        obj.transform.position = new Vector3(start.transform.position.x, start.transform.position.y, 5);
        gameManager.GetComponent<GameManager>().enemiesLeft++;

    }
}
