﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.UI;

public class Node : MonoBehaviour
{

    [SerializeField]
    protected List<Node> connections = new List<Node>();
    [SerializeField]
    protected List<Node> notConnections = new List<Node>();

    private bool isBlock;
    private int nodeToRemove;
    public bool isTower;

    public GameObject gameManager;

    public virtual List<Node> GetConnections
    {
        get
        {
            return connections;
        }
    }

    public virtual List<Node> GetNotConnections
    {
        get
        {
            return notConnections;
        }
    }
    public Node this[int index]
    {
        get
        {
            return connections[index];
        }
    }

    private void OnValidate()
    {
        //remove duplicate elements
        connections = connections.Distinct().ToList();
    }

    public void OnMouseDown()
    {
        if (this.isBlock == false && gameManager.GetComponent<GameManager>().gameState == "Build Maze" && this.gameObject.layer == 0)
        {
            this.isBlock = true;
            this.GetComponent<SpriteRenderer>().color = Color.white;
            for (int i = 0; i < this.connections.Count; i++)
            {
                notConnections.Add(connections[i]);
            }
            connections.Clear();
            for (int i = 0; i < this.notConnections.Count; i++)
            {
                for (int j = 0; j < this.notConnections[i].connections.Count; j++)
                {
                    if (this.notConnections[i].connections[j].isBlock == true)
                    {
                        this.notConnections[i].notConnections.Add(notConnections[i].connections[j]);
                        nodeToRemove = j;
                    }
                    if (j == notConnections[i].connections.Count - 1)
                    {
                        notConnections[i].connections.Remove(notConnections[i].connections[nodeToRemove]);
                    }
                }   
            }
        }
        if (this.isBlock == true && this.isTower == false && gameManager.GetComponent<GameManager>().gameState == "Build Towers" && this.gameObject.layer == 0)
        {
            gameManager.GetComponent<GameManager>().selectedNode = this;
            gameManager.GetComponent<GameManager>().dmgTowerBtn.gameObject.SetActive(true);
            gameManager.GetComponent<GameManager>().slowTowerBtn.gameObject.SetActive(true);
            gameManager.GetComponent<GameManager>().buildTowerBtn.gameObject.SetActive(true);
        }
    }
}
