﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Enemy : MonoBehaviour {

    public float health;
    public float speed;
    private float cachedSpeed;
    public GameObject gameManager;

    private int currentWave;

    private void OnEnable()
    {
    }

    // Update is called once per frame
    void Update()
    {
        if (this.gameObject.GetComponent<Follower>() != null)
        {
            this.gameObject.GetComponent<Follower>().speed = speed;
        }
        if (health <= 0)
        {
            gameManager.GetComponent<GameManager>().enemiesLeft--;
            Destroy(gameObject);
        }
    }

    private void OnTriggerStay(Collider collision)
    {
        if (gameObject.activeSelf && collision.gameObject.layer == 9)
        {
            health -= 0.1f;
            Debug.Log("test");
        }
    }

    private void OnTriggerEnter(Collider collision)
    {
        if (gameObject.activeSelf && collision.gameObject.layer == 10)
        {
            cachedSpeed = speed;
            speed = speed *= 0.50f;
        }
        if (gameObject.activeSelf && collision.gameObject.layer == 11)
        {
            gameManager.GetComponent<GameManager>().currentLives--;
            gameManager.GetComponent<GameManager>().enemiesLeft--;
            Destroy(gameObject);
        }
    }

    private void OnTriggerExit(Collider collision)
    {
        if (gameObject.activeSelf && collision.gameObject.layer == 10)
        {
            speed = cachedSpeed;
        }
    }
}