﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Path
{

    protected List<Node> nodes = new List<Node>();

    protected float length = 0;

    public virtual List<Node> GetNodes
    {
        get { return nodes; }
    }

    public virtual float GetLength
    {
        get { return length; }
    }

    public virtual void Bake()
    {
        List<Node> calculated = new List<Node>();
        length = 0;

        for (int i = 0; i < nodes.Count; i++)
        {
            Node node = nodes[i];

            for (int j = 0; j < node.GetConnections.Count; j++)
            {
                Node connection = node.GetConnections[j];

                if (nodes.Contains(connection) && !calculated.Contains(connection))
                {
                    length += Vector3.Distance(node.transform.position, connection.transform.position);
                }
                calculated.Add(node);
            }
        }
    }

    public override string ToString()
    {
        return string.Format("Nodes: {0}\nLength: {1}", string.Join(",", nodes.Select(node => node.name).ToArray()), length);
    }
}