﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Follower : MonoBehaviour
{

    public Graph graph;
    public Node start;
    public Node end;
    public float speed;

    protected Path path = new Path();
    protected Node current;

    private void Start()
    {
        path = graph.GetDijkstraPath(start, end);
        FollowPath(path);
    }

    public void FollowPath(Path path)
    {
        StopCoroutine(MoveTowardsPath());
        this.path = path;
        transform.position = path.GetNodes[0].transform.position;
        StartCoroutine(MoveTowardsPath());
    }

    IEnumerator MoveTowardsPath()
    {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.update += Update;
#endif

        var e = path.GetNodes.GetEnumerator();
        while (e.MoveNext())
        {
            current = e.Current;

            yield return new WaitUntil(() =>
            {
                return transform.position == current.transform.position;
            });
        }

        current = null;

#if UNITY_EDITOR
        UnityEditor.EditorApplication.update -= Update;
#endif
    }

    private void Update()
    {
        if (current != null && this != null)
        {
            transform.position = Vector3.MoveTowards(transform.position, current.transform.position, speed);
        }
    }
}
